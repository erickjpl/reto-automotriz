<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBilligDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billig_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('quantity')->unsigned();
            $table->double('tax', 10, 2)->unsigned();
            $table->double('price', 10, 2)->unsigned();
            $table->morphs('productable');
            $table->bigInteger('promotion_id')->unsigned()->nullable();
            $table->bigInteger('billig_id')->unsigned();
            $table->foreign('promotion_id')->references('id')->on('promotions')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('billig_id')->references('id')->on('billigs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billig_details');
    }
}
