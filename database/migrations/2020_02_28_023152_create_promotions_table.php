<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->enum('status', ['activo', 'agotado', 'no disponible']);
            $table->enum('promotion_type', ['descuento', 'servicio gratuito']);
            $table->enum('tax_type', ['porcentaje', 'cantidad']);
            $table->bigInteger('concessionaire_id')->unsigned();
            $table->bigInteger('vehicle_id')->unsigned();
            $table->bigInteger('service_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
