<?php

use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('permissions')->delete();

        # Sin Permisos
        Permission::create([
            'name'          =>'api.default',
            'slug'          =>'api.default',
            'description'   =>'api.default',
        ]);

        $models = $arrayName = array(
            'layout',
            'concessionaire',
            'category',
            'brand',
        );

        foreach ($models as $value) {
            Permission::create([
                'name'          =>'api.'.$value.'.index',
                'slug'          =>'api.'.$value.'.index',
                'description'   =>'api.'.$value.'.index',
            ]);
            Permission::create([
                'name'          =>'api.'.$value.'.store',
                'slug'          =>'api.'.$value.'.store',
                'description'   =>'api.'.$value.'.store',
            ]);
            Permission::create([
                'name'          =>'api.'.$value.'.show',
                'slug'          =>'api.'.$value.'.show',
                'description'   =>'api.'.$value.'.show',
            ]);
            Permission::create([
                'name'          =>'api.'.$value.'.update',
                'slug'          =>'api.'.$value.'.update',
                'description'   =>'api.'.$value.'.update',
            ]);
            Permission::create([
                'name'          =>'api.'.$value.'.destroy',
                'slug'          =>'api.'.$value.'.destroy',
                'description'   =>'api.'.$value.'.destroy',
            ]);
        }
    }
}
