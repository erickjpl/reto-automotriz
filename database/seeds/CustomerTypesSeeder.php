<?php

use Illuminate\Database\Seeder;

class CustomerTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customer_types')->delete();

        CustomerType::create([
        	'type' 			=> 'Visitante',
        	'description'	=> 'El usuario se registro desde la aplicación'
        ]);
        CustomerType::create([
        	'type' 			=> 'Standar',
        	'description'	=> 'Nuevo cliente (Realizo la primera compra)'
        ]);
        CustomerType::create([
        	'type' 			=> 'Regular',
        	'description'	=> 'Cumple con los servicios reglamentarios'
        ]);
        CustomerType::create([
        	'type' 			=> 'Neutro',
        	'description'	=> 'Solo realizo la compra o usa el servicio cuando tiene falla el vehiculo'
        ]);
        CustomerType::create([
        	'type' 			=> 'Estelar',
        	'description'	=> 'Ha comprado dos o tres vehiculos'
        ]);
        CustomerType::create([
        	'type' 			=> 'Estrella',
        	'description'	=> 'Ha comprado dos o tres vehiculos y cumple con los servicios reglamentarios'
        ]);
        CustomerType::create([
        	'type' 			=> 'Bronce',
        	'description'	=> 'Ha comprado cuatro o cinco vehiculos'
        ]);
        CustomerType::create([
        	'type' 			=> 'Plata',
        	'description'	=> 'Ha comprado cuatro o cinco vehiculos y cumple con los servicios reglamentarios'
        ]);
        CustomerType::create([
        	'type' 			=> 'Oro',
        	'description'	=> 'Ha comprado mas de cinco vehiculos'
        ]);
        CustomerType::create([
        	'type' 			=> 'VIP',
        	'description'	=> 'Ha comprado mas de cinco vehiculos y cumple con los servicios reglamentarios'
        ]);
    }
}
