<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

        Category::create([
        	'category' => 'Sedan'
        ]);

        Category::create([
        	'category' => 'Rusticos'
        ]);

        Category::create([
        	'category' => 'Camiones'
        ]);
    }
}
