<?php

use App\Models\User;
use App\Models\Profile;
use App\Models\Employee;
use Illuminate\Support\Arr;
use App\Models\Contract;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('employes')->delete();
        DB::table('contracts')->delete();
        DB::table('role_user')->delete();
       
        # ADMINISTRADOR
        $user = User::create([
        	'name' => 'Adminstrador',
	        'email' => 'admin@mail.com',
	        'email_verified_at' => now(),
	        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
	        'remember_token' => Str::random(10),
        ]);

        $user->assignRoles('admin');

        $empleyee = Employee::create([
            'name'          => 'Admin',
            'lastname'      => 'Admin',
            'dni'           => rand(1000000, 39999999),
            'phone'         => '+58 ('.Arr::random([212, 412, 414, 416, 424, 426]).') '.rand(1000000, 9999999),
            'birth'         => date('Y-m-d', rand(strtotime('1960-01-01'), strtotime('2001-12-31'))),
            'status'        => 'nuevo',
            'user_id'       => $user->id
        ]);

        Contract::create([
            'salary'     => 0,
            'contract_date' => date('Y-m-d', rand(strtotime('2018-01-01'), strtotime('2020-12-31'))),
            'employee_id' => $empleyee->id
        ]);
        #################################################################################################

        # GERENTE GENERAL
        $user = User::create([
            'name' => 'myeguez',
            'email' => 'myeguez@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        $user->assignRoles('gerente-general');

        $empleyee = Employee::create([
            'name'          => 'Mariana',
            'lastname'      => 'Yeguez',
            'dni'           => rand(1000000, 99999999),
            'phone'         => '+58 ('.Arr::random([212, 412, 414, 416, 424, 426]).') '.rand(1000000, 9999999),
            'birth'         => date('Y-m-d', rand(strtotime('1960-01-01'), strtotime('2001-12-31'))),
            'status'        => 'nuevo',
            'user_id'       => $user->id
        ]);

        Contract::create([
            'salary'     => 1137.18,
            'contract_date' => date('Y-m-d', rand(strtotime('2018-01-01'), strtotime('2020-12-31'))),
            'employee_id' => $empleyee->id
        ]);
        #################################################################################################

        # GERENTE ADMMINISTRATIVO
        $user = User::create([
            'name' => 'cpiñango',
            'email' => 'cpiñango@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        $user->assignRoles('gerente-administrativo');

        $empleyee = Employee::create([
            'name'          => 'Cruz',
            'lastname'      => 'Piñango',
            'dni'           => rand(1000000, 99999999),
            'phone'         => '+58 ('.Arr::random([212, 412, 414, 416, 424, 426]).') '.rand(1000000, 9999999),
            'birth'         => date('Y-m-d', rand(strtotime('1960-01-01'), strtotime('2001-12-31'))),
            'status'        => 'nuevo',
            'user_id'       => $user->id
        ]);

        Contract::create([
            'salary'     => 1043.57,
            'contract_date' => date('Y-m-d', rand(strtotime('2018-01-01'), strtotime('2020-12-31'))),
            'employee_id' => $empleyee->id
        ]);
        #################################################################################################

        # GERENTE ALMACEN
        $user = User::create([
            'name' => 'aperez',
            'email' => 'aperez@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        $user->assignRoles('gerente-almacen');

        $empleyee = Employee::create([
            'name'          => 'Alirio',
            'lastname'      => 'Perez',
            'dni'           => rand(1000000, 99999999),
            'phone'         => '+58 ('.Arr::random([212, 412, 414, 416, 424, 426]).') '.rand(1000000, 9999999),
            'birth'         => date('Y-m-d', rand(strtotime('1960-01-01'), strtotime('2001-12-31'))),
            'status'        => 'nuevo',
            'user_id'       => $user->id
        ]);

        Contract::create([
            'salary'     => 989.62,
            'contract_date' => date('Y-m-d', rand(strtotime('2018-01-01'), strtotime('2020-12-31'))),
            'employee_id' => $empleyee->id
        ]);
        #################################################################################################

        # GERENTE VENTAS
        $user = User::create([
            'name' => 'pvivas',
            'email' => 'pvivas@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        $user->assignRoles('gerente-ventas');

        $empleyee = Employee::create([
            'name'          => 'Paola',
            'lastname'      => 'Vivas',
            'dni'           => rand(1000000, 99999999),
            'phone'         => '+58 ('.Arr::random([212, 412, 414, 416, 424, 426]).') '.rand(1000000, 9999999),
            'birth'         => date('Y-m-d', rand(strtotime('1960-01-01'), strtotime('2001-12-31'))),
            'status'        => 'nuevo',
            'user_id'       => $user->id
        ]);

        Contract::create([
            'salary'     => 1097.38,
            'contract_date' => date('Y-m-d', rand(strtotime('2018-01-01'), strtotime('2020-12-31'))),
            'employee_id' => $empleyee->id
        ]);
        #################################################################################################

        # PERSONAL ADMINISTRATIVO
        $user = User::create([
            'name' => 'cblaco',
            'email' => 'cblaco@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        $user->assignRoles('personal-administrativo');

        $empleyee = Employee::create([
            'name'          => 'Carolina',
            'lastname'      => 'Blanco',
            'dni'           => rand(1000000, 99999999),
            'phone'         => '+58 ('.Arr::random([212, 412, 414, 416, 424, 426]).') '.rand(1000000, 9999999),
            'birth'         => date('Y-m-d', rand(strtotime('1960-01-01'), strtotime('2001-12-31'))),
            'status'        => 'nuevo',
            'user_id'       => $user->id
        ]);

        Contract::create([
            'salary'     => 834.87,
            'contract_date' => date('Y-m-d', rand(strtotime('2018-01-01'), strtotime('2020-12-31'))),
            'employee_id' => $empleyee->id
        ]);
        #################################################################################################

        # PERSONAL VENTAS
        $user = User::create([
            'name' => 'smendez',
            'email' => 'smendez@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        $user->assignRoles('personal-ventas');

        $empleyee = Employee::create([
            'name'          => 'Susana',
            'lastname'      => 'Mendez',
            'dni'           => rand(1000000, 99999999),
            'phone'         => '+58 ('.Arr::random([212, 412, 414, 416, 424, 426]).') '.rand(1000000, 9999999),
            'birth'         => date('Y-m-d', rand(strtotime('1960-01-01'), strtotime('2001-12-31'))),
            'status'        => 'nuevo',
            'user_id'       => $user->id
        ]);

        Contract::create([
            'salary'     => 877.90,
            'contract_date' => date('Y-m-d', rand(strtotime('2018-01-01'), strtotime('2020-12-31'))),
            'employee_id' => $empleyee->id
        ]);
        #################################################################################################

        # PERSONAL MECANICO
        $user = User::create([
            'name' => 'kperalta',
            'email' => 'kperalta@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        $user->assignRoles('personal-mecanico');

        $empleyee = Employee::create([
            'name'          => 'Kevin',
            'lastname'      => 'Peralta',
            'dni'           => rand(1000000, 99999999),
            'phone'         => '+58 ('.Arr::random([212, 412, 414, 416, 424, 426]).') '.rand(1000000, 9999999),
            'birth'         => date('Y-m-d', rand(strtotime('1960-01-01'), strtotime('2001-12-31'))),
            'status'        => 'nuevo',
            'user_id'       => $user->id
        ]);

        Contract::create([
            'salary'     => 799.96,
            'contract_date' => date('Y-m-d', rand(strtotime('2018-01-01'), strtotime('2020-12-31'))),
            'employee_id' => $empleyee->id
        ]);
        #################################################################################################
    }
}
