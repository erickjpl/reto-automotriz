<?php

use Illuminate\Database\Seeder;
use App\Models\Brand;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->delete();
       
        Brand::create([
        	'brand' => 'Chevrolet'
        ]);

        Brand::create([
        	'brand' => 'Toyota'
        ]);

        Brand::create([
        	'brand' => 'Ford'
        ]);

        Brand::create([
        	'brand' => 'Hundai'
        ]);
    }
}
