<?php

use Caffeinated\Shinobi\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        Role::create([
			'name' 			=> 'Admin',
			'slug' 			=> 'admin',
			'description' 	=> 'Admin',
			'special' 		=> 'all-access'
		]);

        Role::create([
			'name' 			=> 'Gerente General',
			'slug' 			=> 'gerente-general',
			'description' 	=> 'Gerente General'
		]);

        Role::create([
			'name' 			=> 'Gerente Administrativo',
			'slug' 			=> 'gerente-administrativo',
			'description' 	=> 'Gerente Administrativo'
		]);

        Role::create([
			'name' 			=> 'Gerente de Almacen',
			'slug' 			=> 'gerente-almacen',
			'description' 	=> 'Gerente de Almacen'
		]);

        Role::create([
			'name' 			=> 'Gerente de Ventas',
			'slug' 			=> 'gerente-ventas',
			'description' 	=> 'Gerente de Ventas'
		]);

		Role::create([
			'name' 			=> 'Personal Administrativo',
			'slug' 			=> 'personal-administrativo',
			'description' 	=> 'Personal Administrativo'
		]);

		Role::create([
			'name' 			=> 'Personal de Ventas',
			'slug' 			=> 'personal-ventas',
			'description' 	=> 'Personal de Ventas'
		]);

		Role::create([
			'name' 			=> 'Personal Mecanico',
			'slug' 			=> 'personal-mecanico',
			'description' 	=> 'Personal Mecanico'
		]);

		Role::create([
			'name'			=> 'Cliente',
			'slug'			=> 'guest',
			'description' 	=> 'Cliente'
		]);
    }
}
