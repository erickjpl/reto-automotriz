<?php

use Illuminate\Database\Seeder;

class ConcessionairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('concessionaires')->delete();

        factory(App\Models\Concessionaire::class, 29)->create();
    }
}
