<?php

use App\Models\City;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->delete();

    	# Amazonas
        City::create([
			'city' => 'Maroa',
			'slug' => \Str::slug('Maroa', '-'),
			'state_id' => 1,	
			'capital' => 0
		]);
        City::create([
			'city' => 'Puerto Ayacucho',
			'slug' => \Str::slug('Puerto Ayacucho', '-'),
			'state_id' => 1,	
			'capital' => 1
		]);
        City::create([
			'city' => 'San Fernando de Atabapo',
			'slug' => \Str::slug('San Fernando de Atabapo', '-'),
			'state_id' => 1,	
			'capital' => 0
		]);


        # Anzoátegui
		City::create([
			'city' => 'Anaco',
			'slug' => \Str::slug('Anaco', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Aragua de Barcelona',
			'slug' => \Str::slug('Aragua de Barcelona', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Barcelona',
			'slug' => \Str::slug('Barcelona', '-'),
			'state_id' => 2,
			'capital' => 1,
		]);
		City::create([
			'city' => 'Boca de Uchire',
			'slug' => \Str::slug('Boca de Uchire', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Cantaura',
			'slug' => \Str::slug('Cantaura', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Clarines',
			'slug' => \Str::slug('Clarines', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'El Chaparro',
			'slug' => \Str::slug('El Chaparro', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'El Pao Anzoátegui',
			'slug' => \Str::slug('El Pao Anzoátegui', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'El Tigre',
			'slug' => \Str::slug('El Tigre', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'El Tigrito',
			'slug' => \Str::slug('El Tigrito', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Guanape',
			'slug' => \Str::slug('Guanape', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Guanta',
			'slug' => \Str::slug('Guanta', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Lechería',
			'slug' => \Str::slug('Lechería', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Onoto',
			'slug' => \Str::slug('Onoto', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Pariaguán',
			'slug' => \Str::slug('Pariaguán', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Píritu',
			'slug' => \Str::slug('Píritu', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Puerto La Cruz',
			'slug' => \Str::slug('Puerto La Cruz', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Puerto Píritu',
			'slug' => \Str::slug('Puerto Píritu', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Sabana de Uchire',
			'slug' => \Str::slug('Sabana de Uchire', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'San Mateo Anzoátegui',
			'slug' => \Str::slug('San Mateo Anzoátegui', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'San Pablo Anzoátegui',
			'slug' => \Str::slug('San Pablo Anzoátegui', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'San Tomé',
			'slug' => \Str::slug('San Tomé', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Santa Ana de Anzoátegui',
			'slug' => \Str::slug('Santa Ana de Anzoátegui', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Santa Fe Anzoátegui',
			'slug' => \Str::slug('Santa Fe Anzoátegui', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Santa Rosa',
			'slug' => \Str::slug('Santa Rosa', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Soledad',
			'slug' => \Str::slug('Soledad', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Urica',
			'slug' => \Str::slug('Urica', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Valle de Guanape',
			'slug' => \Str::slug('Valle de Guanape', '-'),
			'state_id' => 2,
			'capital' => 0,
		]);


		# Apure
		City::create([
			'city' => 'Achaguas',
			'slug' => \Str::slug('Achaguas', '-'),
			'state_id' => 3,
			'capital' => 0
		]);
		City::create([
			'city' => 'Biruaca',
			'slug' => \Str::slug('Biruaca', '-'),
			'state_id' => 3,
			'capital' => 0
		]);
		City::create([
			'city' => 'Bruzual',
			'slug' => \Str::slug('Bruzual', '-'),
			'state_id' => 3,
			'capital' => 0
		]);
		City::create([
			'city' => 'El Amparo',
			'slug' => \Str::slug('El Amparo', '-'),
			'state_id' => 3,
			'capital' => 0
		]);
		City::create([
			'city' => 'El Nula',
			'slug' => \Str::slug('El Nula', '-'),
			'state_id' => 3,
			'capital' => 0
		]);
		City::create([
			'city' => 'Elorza',
			'slug' => \Str::slug('Elorza', '-'),
			'state_id' => 3,
			'capital' => 0
		]);
		City::create([
			'city' => 'Guasdualito',
			'slug' => \Str::slug('Guasdualito', '-'),
			'state_id' => 3,
			'capital' => 0
		]);
		City::create([
			'city' => 'Mantecal',
			'slug' => \Str::slug('Mantecal', '-'),
			'state_id' => 3,
			'capital' => 0
		]);
		City::create([
			'city' => 'Puerto Páez',
			'slug' => \Str::slug('Puerto Páez', '-'),
			'state_id' => 3,
			'capital' => 0
		]);
		City::create([
			'city' => 'San Fernando de Apure',
			'slug' => \Str::slug('San Fernando de Apure', '-'),
			'state_id' => 3,
			'capital' => 1
		]);
		City::create([
			'city' => 'San Juan de Payara',
			'slug' => \Str::slug('San Juan de Payara', '-'),
			'state_id' => 3,
			'capital' => 0
		]);


		# Aragua
		City::create([
			'city' => 'Barbacoas', 
			'slug' => \Str::slug('Barbacoas', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Cagua', 
			'slug' => \Str::slug('Cagua', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Camatagua', 
			'slug' => \Str::slug('Camatagua', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Choroní', 
			'slug' => \Str::slug('Choroní', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Colonia Tovar', 
			'slug' => \Str::slug('Colonia Tovar', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'El Consejo', 
			'slug' => \Str::slug('El Consejo', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'La Victoria', 
			'slug' => \Str::slug('La Victoria', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Las Tejerías', 
			'slug' => \Str::slug('Las Tejerías', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Magdaleno', 
			'slug' => \Str::slug('Magdaleno', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Maracay', 
			'slug' => \Str::slug('Maracay', '-'),
			'state_id' => 4,
			'capital' => 1,
		]);
		City::create([
			'city' => 'Ocumare de La Costa', 
			'slug' => \Str::slug('Ocumare de La Costa', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Palo Negro', 
			'slug' => \Str::slug('Palo Negro', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'San Casimiro', 
			'slug' => \Str::slug('San Casimiro', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'San Mateo', 
			'slug' => \Str::slug('San Mateo', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'San Sebastián', 
			'slug' => \Str::slug('San Sebastián', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Santa Cruz de Aragua', 
			'slug' => \Str::slug('Santa Cruz de Aragua', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Tocorón', 
			'slug' => \Str::slug('Tocorón', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
			'city' => 'Turmero', 
			'slug' => \Str::slug('Turmero', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
		'city' => 'Villa de Cura', 
		'slug' => \Str::slug('Villa de Cura', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);
		City::create([
		'city' => 'Zuata', 
		'slug' => \Str::slug('Zuata', '-'),
			'state_id' => 4,
			'capital' => 0,
		]);

		# Barinas
		City::create([
		    'city' => 'Barinas', 
		    'slug' => \Str::slug('Barinas', '-'),
		    'state_id' => 5,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Barinitas', 
		    'slug' => \Str::slug('Barinitas', '-'),
		    'state_id' => 5,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Barrancas', 
		    'slug' => \Str::slug('Barrancas', '-'),
		    'state_id' => 5,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Calderas', 
		    'slug' => \Str::slug('Calderas', '-'),
		    'state_id' => 5,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Capitanejo', 
		    'slug' => \Str::slug('Capitanejo', '-'),
		    'state_id' => 5,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Ciudad Bolivia', 
		    'slug' => \Str::slug('Ciudad Bolivia', '-'),
		    'state_id' => 5,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Cantón', 
		    'slug' => \Str::slug('El Cantón', '-'),
		    'state_id' => 5,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Las Veguitas', 
		    'slug' => \Str::slug('Las Veguitas', '-'),
		    'state_id' => 5,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Libertad de Barinas', 
		    'slug' => \Str::slug('Libertad de Barinas', '-'),
		    'state_id' => 5,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Sabaneta', 
		    'slug' => \Str::slug('Sabaneta', '-'),
		    'state_id' => 5,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Bárbara de Barinas', 
		    'slug' => \Str::slug('Santa Bárbara de Barinas', '-'),
		    'state_id' => 5,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Socopó', 
		    'slug' => \Str::slug('Socopó', '-'),
		    'state_id' => 5,
		    'capital' => 0
		]);

		
		# Bolívar
		City::create([
		    'city' => 'Caicara del Orinoco', 
		    'slug' => \Str::slug('Caicara del Orinoco', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Canaima', 
		    'slug' => \Str::slug('Canaima', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Ciudad Bolívar', 
		    'slug' => \Str::slug('Ciudad Bolívar', '-'),
		    'state_id' => 6,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Ciudad Piar', 
		    'slug' => \Str::slug('Ciudad Piar', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Callao', 
		    'slug' => \Str::slug('El Callao', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Dorado', 
		    'slug' => \Str::slug('El Dorado', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Manteco', 
		    'slug' => \Str::slug('El Manteco', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Palmar', 
		    'slug' => \Str::slug('El Palmar', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Pao', 
		    'slug' => \Str::slug('El Pao', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Guasipati', 
		    'slug' => \Str::slug('Guasipati', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Guri', 
		    'slug' => \Str::slug('Guri', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Paragua', 
		    'slug' => \Str::slug('La Paragua', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Matanzas', 
		    'slug' => \Str::slug('Matanzas', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Puerto Ordaz', 
		    'slug' => \Str::slug('Puerto Ordaz', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Félix', 
		    'slug' => \Str::slug('San Félix', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Elena de Uairén', 
		    'slug' => \Str::slug('Santa Elena de Uairén', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tumeremo', 
		    'slug' => \Str::slug('Tumeremo', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Unare', 
		    'slug' => \Str::slug('Unare', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Upata', 
		    'slug' => \Str::slug('Upata', '-'),
		    'state_id' => 6,
		    'capital' => 0
		]);


		# Carabobo
		City::create([
		    'city' => 'Bejuma', 
		    'slug' => \Str::slug('Bejuma', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Belén', 
		    'slug' => \Str::slug('Belén', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Campo de Carabobo', 
		    'slug' => \Str::slug('Campo de Carabobo', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Canoabo', 
		    'slug' => \Str::slug('Canoabo', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Central Tacarigua', 
		    'slug' => \Str::slug('Central Tacarigua', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chirgua', 
		    'slug' => \Str::slug('Chirgua', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Ciudad Alianza', 
		    'slug' => \Str::slug('Ciudad Alianza', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Palito', 
		    'slug' => \Str::slug('El Palito', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Guacara', 
		    'slug' => \Str::slug('Guacara', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Guigue', 
		    'slug' => \Str::slug('Guigue', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Las Trincheras', 
		    'slug' => \Str::slug('Las Trincheras', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Los Guayos', 
		    'slug' => \Str::slug('Los Guayos', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Mariara', 
		    'slug' => \Str::slug('Mariara', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Miranda', 
		    'slug' => \Str::slug('Miranda', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Montalbán', 
		    'slug' => \Str::slug('Montalbán', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Morón', 
		    'slug' => \Str::slug('Morón', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Naguanagua', 
		    'slug' => \Str::slug('Naguanagua', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Puerto Cabello', 
		    'slug' => \Str::slug('Puerto Cabello', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Joaquín', 
		    'slug' => \Str::slug('San Joaquín', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tocuyito', 
		    'slug' => \Str::slug('Tocuyito', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Urama', 
		    'slug' => \Str::slug('Urama', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Valencia', 
		    'slug' => \Str::slug('Valencia', '-'),
		    'state_id' => 7,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Vigirimita', 
		    'slug' => \Str::slug('Vigirimita', '-'),
		    'state_id' => 7,
		    'capital' => 0
		]);


		# Cojedes
		City::create([
		    'city' => 'Aguirre', 
		    'slug' => \Str::slug('Aguirre', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Apartaderos Cojedes', 
		    'slug' => \Str::slug('Apartaderos Cojedes', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Arismendi', 
		    'slug' => \Str::slug('Arismendi', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Camuriquito', 
		    'slug' => \Str::slug('Camuriquito', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Baúl', 
		    'slug' => \Str::slug('El Baúl', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Limón', 
		    'slug' => \Str::slug('El Limón', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Pao Cojedes', 
		    'slug' => \Str::slug('El Pao Cojedes', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Socorro', 
		    'slug' => \Str::slug('El Socorro', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Aguadita', 
		    'slug' => \Str::slug('La Aguadita', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Las Vegas', 
		    'slug' => \Str::slug('Las Vegas', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Libertad de Cojedes', 
		    'slug' => \Str::slug('Libertad de Cojedes', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Mapuey', 
		    'slug' => \Str::slug('Mapuey', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Piñedo', 
		    'slug' => \Str::slug('Piñedo', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Samancito', 
		    'slug' => \Str::slug('Samancito', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Carlos', 
		    'slug' => \Str::slug('San Carlos', '-'),
		    'state_id' => 8,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Sucre', 
		    'slug' => \Str::slug('Sucre', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tinaco', 
		    'slug' => \Str::slug('Tinaco', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tinaquillo', 
		    'slug' => \Str::slug('Tinaquillo', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Vallecito', 
		    'slug' => \Str::slug('Vallecito', '-'),
		    'state_id' => 8,
		    'capital' => 0
		]);


		# Delta Amacuro
		City::create([
			'city' => 'Tucupita',
			'slug' => \Str::slug('Tucupita', '-'),
			'state_id' => 9,
			'capital' => 1
		]);

		# Distrito Capital
		City::create([
		    'city' => 'Caracas', 
		    'slug' => \Str::slug('Caracas', '-'),
		    'state_id' => 10,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'El Junquito', 
		    'slug' => \Str::slug('El Junquito', '-'),
		    'state_id' => 10,
		    'capital' => 0
		]);

		# Falcón
		City::create([
		    'city' => 'Adícora', 
		    'slug' => \Str::slug('Adícora', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Boca de Aroa', 
		    'slug' => \Str::slug('Boca de Aroa', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cabure', 
		    'slug' => \Str::slug('Cabure', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Capadare', 
		    'slug' => \Str::slug('Capadare', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Capatárida', 
		    'slug' => \Str::slug('Capatárida', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chichiriviche', 
		    'slug' => \Str::slug('Chichiriviche', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Churuguara', 
		    'slug' => \Str::slug('Churuguara', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Coro', 
		    'slug' => \Str::slug('Coro', '-'),
		    'state_id' => 11,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Cumarebo', 
		    'slug' => \Str::slug('Cumarebo', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Dabajuro', 
		    'slug' => \Str::slug('Dabajuro', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Judibana', 
		    'slug' => \Str::slug('Judibana', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Cruz de Taratara', 
		    'slug' => \Str::slug('La Cruz de Taratara', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Vela de Coro', 
		    'slug' => \Str::slug('La Vela de Coro', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Los Taques', 
		    'slug' => \Str::slug('Los Taques', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Maparari', 
		    'slug' => \Str::slug('Maparari', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Mene de Mauroa', 
		    'slug' => \Str::slug('Mene de Mauroa', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Mirimire', 
		    'slug' => \Str::slug('Mirimire', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Pedregal', 
		    'slug' => \Str::slug('Pedregal', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Píritu Falcón', 
		    'slug' => \Str::slug('Píritu Falcón', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Pueblo Nuevo Falcón', 
		    'slug' => \Str::slug('Pueblo Nuevo Falcón', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Puerto Cumarebo', 
		    'slug' => \Str::slug('Puerto Cumarebo', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Punta Cardón', 
		    'slug' => \Str::slug('Punta Cardón', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Punto Fijo', 
		    'slug' => \Str::slug('Punto Fijo', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Juan de Los Cayos', 
		    'slug' => \Str::slug('San Juan de Los Cayos', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Luis', 
		    'slug' => \Str::slug('San Luis', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Ana Falcón', 
		    'slug' => \Str::slug('Santa Ana Falcón', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Cruz De Bucaral', 
		    'slug' => \Str::slug('Santa Cruz De Bucaral', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tocopero', 
		    'slug' => \Str::slug('Tocopero', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tocuyo de La Costa', 
		    'slug' => \Str::slug('Tocuyo de La Costa', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tucacas', 
		    'slug' => \Str::slug('Tucacas', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Yaracal', 
		    'slug' => \Str::slug('Yaracal', '-'),
		    'state_id' => 11,
		    'capital' => 0
		]);


		# Guárico
		City::create([
		    'city' => 'Altagracia de Orituco', 
		    'slug' => \Str::slug('Altagracia de Orituco', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cabruta', 
		    'slug' => \Str::slug('Cabruta', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Calabozo', 
		    'slug' => \Str::slug('Calabozo', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Camaguán', 
		    'slug' => \Str::slug('Camaguán', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chaguaramas Guárico', 
		    'slug' => \Str::slug('Chaguaramas Guárico', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Socorro', 
		    'slug' => \Str::slug('El Socorro', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Sombrero', 
		    'slug' => \Str::slug('El Sombrero', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Las Mercedes de Los Llanos', 
		    'slug' => \Str::slug('Las Mercedes de Los Llanos', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Lezama', 
		    'slug' => \Str::slug('Lezama', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Onoto', 
		    'slug' => \Str::slug('Onoto', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Ortíz', 
		    'slug' => \Str::slug('Ortíz', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San José de Guaribe', 
		    'slug' => \Str::slug('San José de Guaribe', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Juan de Los Morros', 
		    'slug' => \Str::slug('San Juan de Los Morros', '-'),
		    'state_id' => 12,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'San Rafael de Laya', 
		    'slug' => \Str::slug('San Rafael de Laya', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa María de Ipire', 
		    'slug' => \Str::slug('Santa María de Ipire', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tucupido', 
		    'slug' => \Str::slug('Tucupido', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Valle de La Pascua', 
		    'slug' => \Str::slug('Valle de La Pascua', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Zaraza', 
		    'slug' => \Str::slug('Zaraza', '-'),
		    'state_id' => 12,
		    'capital' => 0
		]);


		# Lara
		City::create([
		    'city' => 'Aguada Grande', 
		    'slug' => \Str::slug('Aguada Grande', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Atarigua', 
		    'slug' => \Str::slug('Atarigua', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Barquisimeto', 
		    'slug' => \Str::slug('Barquisimeto', '-'),
		    'state_id' => 13,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Bobare', 
		    'slug' => \Str::slug('Bobare', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cabudare', 
		    'slug' => \Str::slug('Cabudare', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Carora', 
		    'slug' => \Str::slug('Carora', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cubiro', 
		    'slug' => \Str::slug('Cubiro', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cují', 
		    'slug' => \Str::slug('Cují', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Duaca', 
		    'slug' => \Str::slug('Duaca', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Manzano', 
		    'slug' => \Str::slug('El Manzano', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Tocuyo', 
		    'slug' => \Str::slug('El Tocuyo', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Guaríco', 
		    'slug' => \Str::slug('Guaríco', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Humocaro Alto', 
		    'slug' => \Str::slug('Humocaro Alto', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Humocaro Bajo', 
		    'slug' => \Str::slug('Humocaro Bajo', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Miel', 
		    'slug' => \Str::slug('La Miel', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Moroturo', 
		    'slug' => \Str::slug('Moroturo', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Quíbor', 
		    'slug' => \Str::slug('Quíbor', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Río Claro', 
		    'slug' => \Str::slug('Río Claro', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Sanare', 
		    'slug' => \Str::slug('Sanare', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Inés', 
		    'slug' => \Str::slug('Santa Inés', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Sarare', 
		    'slug' => \Str::slug('Sarare', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Siquisique', 
		    'slug' => \Str::slug('Siquisique', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tintorero', 
		    'slug' => \Str::slug('Tintorero', '-'),
		    'state_id' => 13,
		    'capital' => 0
		]);


		# Mérida
		City::create([
		    'city' => 'Apartaderos Mérida', 
		    'slug' => \Str::slug('Apartaderos Mérida', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Arapuey', 
		    'slug' => \Str::slug('Arapuey', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Bailadores', 
		    'slug' => \Str::slug('Bailadores', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Caja Seca', 
		    'slug' => \Str::slug('Caja Seca', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Canaguá', 
		    'slug' => \Str::slug('Canaguá', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chachopo', 
		    'slug' => \Str::slug('Chachopo', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chiguara', 
		    'slug' => \Str::slug('Chiguara', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Ejido', 
		    'slug' => \Str::slug('Ejido', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Vigía', 
		    'slug' => \Str::slug('El Vigía', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Azulita', 
		    'slug' => \Str::slug('La Azulita', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Playa', 
		    'slug' => \Str::slug('La Playa', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Lagunillas Mérida', 
		    'slug' => \Str::slug('Lagunillas Mérida', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Mérida', 
		    'slug' => \Str::slug('Mérida', '-'),
		    'state_id' => 14,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Mesa de Bolívar', 
		    'slug' => \Str::slug('Mesa de Bolívar', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Mucuchíes', 
		    'slug' => \Str::slug('Mucuchíes', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Mucujepe', 
		    'slug' => \Str::slug('Mucujepe', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Mucuruba', 
		    'slug' => \Str::slug('Mucuruba', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Nueva Bolivia', 
		    'slug' => \Str::slug('Nueva Bolivia', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Palmarito', 
		    'slug' => \Str::slug('Palmarito', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Pueblo Llano', 
		    'slug' => \Str::slug('Pueblo Llano', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Cruz de Mora', 
		    'slug' => \Str::slug('Santa Cruz de Mora', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Elena de Arenales', 
		    'slug' => \Str::slug('Santa Elena de Arenales', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santo Domingo', 
		    'slug' => \Str::slug('Santo Domingo', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tabáy', 
		    'slug' => \Str::slug('Tabáy', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Timotes', 
		    'slug' => \Str::slug('Timotes', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Torondoy', 
		    'slug' => \Str::slug('Torondoy', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tovar', 
		    'slug' => \Str::slug('Tovar', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tucani', 
		    'slug' => \Str::slug('Tucani', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Zea', 
		    'slug' => \Str::slug('Zea', '-'),
		    'state_id' => 14,
		    'capital' => 0
		]);


		# Miranda
		City::create([
		    'city' => 'Araguita', 
		    'slug' => \Str::slug('Araguita', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Carrizal', 
		    'slug' => \Str::slug('Carrizal', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Caucagua', 
		    'slug' => \Str::slug('Caucagua', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chaguaramas Miranda', 
		    'slug' => \Str::slug('Chaguaramas Miranda', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Charallave', 
		    'slug' => \Str::slug('Charallave', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chirimena', 
		    'slug' => \Str::slug('Chirimena', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chuspa', 
		    'slug' => \Str::slug('Chuspa', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cúa', 
		    'slug' => \Str::slug('Cúa', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cupira', 
		    'slug' => \Str::slug('Cupira', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Curiepe', 
		    'slug' => \Str::slug('Curiepe', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Guapo', 
		    'slug' => \Str::slug('El Guapo', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Jarillo', 
		    'slug' => \Str::slug('El Jarillo', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Filas de Mariche', 
		    'slug' => \Str::slug('Filas de Mariche', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Guarenas', 
		    'slug' => \Str::slug('Guarenas', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Guatire', 
		    'slug' => \Str::slug('Guatire', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Higuerote', 
		    'slug' => \Str::slug('Higuerote', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Los Anaucos', 
		    'slug' => \Str::slug('Los Anaucos', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Los Teques', 
		    'slug' => \Str::slug('Los Teques', '-'),
		    'state_id' => 15,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Ocumare del Tuy', 
		    'slug' => \Str::slug('Ocumare del Tuy', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Panaquire', 
		    'slug' => \Str::slug('Panaquire', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Paracotos', 
		    'slug' => \Str::slug('Paracotos', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Río Chico', 
		    'slug' => \Str::slug('Río Chico', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Antonio de Los Altos', 
		    'slug' => \Str::slug('San Antonio de Los Altos', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Diego de Los Altos', 
		    'slug' => \Str::slug('San Diego de Los Altos', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Fernando del Guapo', 
		    'slug' => \Str::slug('San Fernando del Guapo', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Francisco de Yare', 
		    'slug' => \Str::slug('San Francisco de Yare', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San José de Los Altos', 
		    'slug' => \Str::slug('San José de Los Altos', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San José de Río Chico', 
		    'slug' => \Str::slug('San José de Río Chico', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Pedro de Los Altos', 
		    'slug' => \Str::slug('San Pedro de Los Altos', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Lucía', 
		    'slug' => \Str::slug('Santa Lucía', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Teresa', 
		    'slug' => \Str::slug('Santa Teresa', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tacarigua de La Laguna', 
		    'slug' => \Str::slug('Tacarigua de La Laguna', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tacarigua de Mamporal', 
		    'slug' => \Str::slug('Tacarigua de Mamporal', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tácata', 
		    'slug' => \Str::slug('Tácata', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Turumo', 
		    'slug' => \Str::slug('Turumo', '-'),
		    'state_id' => 15,
		    'capital' => 0
		]);


		# Monagas
		City::create([
		    'city' => 'Aguasay', 
		    'slug' => \Str::slug('Aguasay', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Aragua de Maturín', 
		    'slug' => \Str::slug('Aragua de Maturín', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Barrancas del Orinoco', 
		    'slug' => \Str::slug('Barrancas del Orinoco', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Caicara de Maturín', 
		    'slug' => \Str::slug('Caicara de Maturín', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Caripe', 
		    'slug' => \Str::slug('Caripe', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Caripito', 
		    'slug' => \Str::slug('Caripito', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chaguaramal', 
		    'slug' => \Str::slug('Chaguaramal', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chaguaramas Monagas', 
		    'slug' => \Str::slug('Chaguaramas Monagas', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Furrial', 
		    'slug' => \Str::slug('El Furrial', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Tejero', 
		    'slug' => \Str::slug('El Tejero', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Jusepín', 
		    'slug' => \Str::slug('Jusepín', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Toscana', 
		    'slug' => \Str::slug('La Toscana', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Maturín', 
		    'slug' => \Str::slug('Maturín', '-'),
		    'state_id' => 16,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Miraflores', 
		    'slug' => \Str::slug('Miraflores', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Punta de Mata', 
		    'slug' => \Str::slug('Punta de Mata', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Quiriquire', 
		    'slug' => \Str::slug('Quiriquire', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Antonio de Maturín', 
		    'slug' => \Str::slug('San Antonio de Maturín', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Vicente Monagas', 
		    'slug' => \Str::slug('San Vicente Monagas', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Bárbara', 
		    'slug' => \Str::slug('Santa Bárbara', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Temblador', 
		    'slug' => \Str::slug('Temblador', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Teresen', 
		    'slug' => \Str::slug('Teresen', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Uracoa', 
		    'slug' => \Str::slug('Uracoa', '-'),
		    'state_id' => 16,
		    'capital' => 0
		]);


		# Nueva Esparta
		City::create([
		    'city' => 'Altagracia', 
		    'slug' => \Str::slug('Altagracia', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Boca de Pozo', 
		    'slug' => \Str::slug('Boca de Pozo', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Boca de Río', 
		    'slug' => \Str::slug('Boca de Río', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Espinal', 
		    'slug' => \Str::slug('El Espinal', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Valle del Espíritu Santo', 
		    'slug' => \Str::slug('El Valle del Espíritu Santo', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Yaque', 
		    'slug' => \Str::slug('El Yaque', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Juangriego', 
		    'slug' => \Str::slug('Juangriego', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Asunción', 
		    'slug' => \Str::slug('La Asunción', '-'),
		    'state_id' => 17,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'La Guardia', 
		    'slug' => \Str::slug('La Guardia', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Pampatar', 
		    'slug' => \Str::slug('Pampatar', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Porlamar', 
		    'slug' => \Str::slug('Porlamar', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Puerto Fermín', 
		    'slug' => \Str::slug('Puerto Fermín', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Punta de Piedras', 
		    'slug' => \Str::slug('Punta de Piedras', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Francisco de Macanao', 
		    'slug' => \Str::slug('San Francisco de Macanao', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Juan Bautista', 
		    'slug' => \Str::slug('San Juan Bautista', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Pedro de Coche', 
		    'slug' => \Str::slug('San Pedro de Coche', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Ana de Nueva Esparta', 
		    'slug' => \Str::slug('Santa Ana de Nueva Esparta', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Villa Rosa', 
		    'slug' => \Str::slug('Villa Rosa', '-'),
		    'state_id' => 17,
		    'capital' => 0
		]);


		# Portuguesa
		City::create([
		    'city' => 'Acarigua', 
		    'slug' => \Str::slug('Acarigua', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Agua Blanca', 
		    'slug' => \Str::slug('Agua Blanca', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Araure', 
		    'slug' => \Str::slug('Araure', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Biscucuy', 
		    'slug' => \Str::slug('Biscucuy', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Boconoito', 
		    'slug' => \Str::slug('Boconoito', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Campo Elías', 
		    'slug' => \Str::slug('Campo Elías', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chabasquén', 
		    'slug' => \Str::slug('Chabasquén', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Guanare', 
		    'slug' => \Str::slug('Guanare', '-'),
		    'state_id' => 18,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Guanarito', 
		    'slug' => \Str::slug('Guanarito', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Aparición', 
		    'slug' => \Str::slug('La Aparición', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Misión', 
		    'slug' => \Str::slug('La Misión', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Mesa de Cavacas', 
		    'slug' => \Str::slug('Mesa de Cavacas', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Ospino', 
		    'slug' => \Str::slug('Ospino', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Papelón', 
		    'slug' => \Str::slug('Papelón', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Payara', 
		    'slug' => \Str::slug('Payara', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Pimpinela', 
		    'slug' => \Str::slug('Pimpinela', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Píritu de Portuguesa', 
		    'slug' => \Str::slug('Píritu de Portuguesa', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Rafael de Onoto', 
		    'slug' => \Str::slug('San Rafael de Onoto', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Rosalía', 
		    'slug' => \Str::slug('Santa Rosalía', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Turén', 
		    'slug' => \Str::slug('Turén', '-'),
		    'state_id' => 18,
		    'capital' => 0
		]);


		# Sucre
		City::create([
		    'city' => 'Altos de Sucre', 
		    'slug' => \Str::slug('Altos de Sucre', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Araya', 
		    'slug' => \Str::slug('Araya', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cariaco', 
		    'slug' => \Str::slug('Cariaco', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Carúpano', 
		    'slug' => \Str::slug('Carúpano', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Casanay', 
		    'slug' => \Str::slug('Casanay', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cumaná', 
		    'slug' => \Str::slug('Cumaná', '-'),
		    'state_id' => 19,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Cumanacoa', 
		    'slug' => \Str::slug('Cumanacoa', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Morro Puerto Santo', 
		    'slug' => \Str::slug('El Morro Puerto Santo', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Pilar', 
		    'slug' => \Str::slug('El Pilar', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Poblado', 
		    'slug' => \Str::slug('El Poblado', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Guaca', 
		    'slug' => \Str::slug('Guaca', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Guiria', 
		    'slug' => \Str::slug('Guiria', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Irapa', 
		    'slug' => \Str::slug('Irapa', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Manicuare', 
		    'slug' => \Str::slug('Manicuare', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Mariguitar', 
		    'slug' => \Str::slug('Mariguitar', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Río Caribe', 
		    'slug' => \Str::slug('Río Caribe', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Antonio del Golfo', 
		    'slug' => \Str::slug('San Antonio del Golfo', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San José de Aerocuar', 
		    'slug' => \Str::slug('San José de Aerocuar', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Vicente de Sucre', 
		    'slug' => \Str::slug('San Vicente de Sucre', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Fe de Sucre', 
		    'slug' => \Str::slug('Santa Fe de Sucre', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tunapuy', 
		    'slug' => \Str::slug('Tunapuy', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Yaguaraparo', 
		    'slug' => \Str::slug('Yaguaraparo', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Yoco', 
		    'slug' => \Str::slug('Yoco', '-'),
		    'state_id' => 19,
		    'capital' => 0
		]);


		# Táchira
		City::create([
		    'city' => 'Abejales', 
		    'slug' => \Str::slug('Abejales', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Borota', 
		    'slug' => \Str::slug('Borota', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Bramon', 
		    'slug' => \Str::slug('Bramon', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Capacho', 
		    'slug' => \Str::slug('Capacho', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Colón', 
		    'slug' => \Str::slug('Colón', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Coloncito', 
		    'slug' => \Str::slug('Coloncito', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cordero', 
		    'slug' => \Str::slug('Cordero', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Cobre', 
		    'slug' => \Str::slug('El Cobre', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Pinal', 
		    'slug' => \Str::slug('El Pinal', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Independencia', 
		    'slug' => \Str::slug('Independencia', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Fría', 
		    'slug' => \Str::slug('La Fría', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Grita', 
		    'slug' => \Str::slug('La Grita', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Pedrera', 
		    'slug' => \Str::slug('La Pedrera', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Tendida', 
		    'slug' => \Str::slug('La Tendida', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Las Delicias', 
		    'slug' => \Str::slug('Las Delicias', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Las Hernández', 
		    'slug' => \Str::slug('Las Hernández', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Lobatera', 
		    'slug' => \Str::slug('Lobatera', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Michelena', 
		    'slug' => \Str::slug('Michelena', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Palmira', 
		    'slug' => \Str::slug('Palmira', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Pregonero', 
		    'slug' => \Str::slug('Pregonero', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Queniquea', 
		    'slug' => \Str::slug('Queniquea', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Rubio', 
		    'slug' => \Str::slug('Rubio', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Antonio del Tachira', 
		    'slug' => \Str::slug('San Antonio del Tachira', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Cristobal', 
		    'slug' => \Str::slug('San Cristobal', '-'),
		    'state_id' => 20,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'San José de Bolívar', 
		    'slug' => \Str::slug('San José de Bolívar', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Josecito', 
		    'slug' => \Str::slug('San Josecito', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Pedro del Río', 
		    'slug' => \Str::slug('San Pedro del Río', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Ana Táchira', 
		    'slug' => \Str::slug('Santa Ana Táchira', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Seboruco', 
		    'slug' => \Str::slug('Seboruco', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Táriba', 
		    'slug' => \Str::slug('Táriba', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Umuquena', 
		    'slug' => \Str::slug('Umuquena', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Ureña', 
		    'slug' => \Str::slug('Ureña', '-'),
		    'state_id' => 20,
		    'capital' => 0
		]);


		# Trujillo
		City::create([
		    'city' => 'Batatal', 
		    'slug' => \Str::slug('Batatal', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Betijoque', 
		    'slug' => \Str::slug('Betijoque', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Boconó', 
		    'slug' => \Str::slug('Boconó', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Carache', 
		    'slug' => \Str::slug('Carache', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chejende', 
		    'slug' => \Str::slug('Chejende', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cuicas', 
		    'slug' => \Str::slug('Cuicas', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Dividive', 
		    'slug' => \Str::slug('El Dividive', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Jaguito', 
		    'slug' => \Str::slug('El Jaguito', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Escuque', 
		    'slug' => \Str::slug('Escuque', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Isnotú', 
		    'slug' => \Str::slug('Isnotú', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Jajó', 
		    'slug' => \Str::slug('Jajó', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Ceiba', 
		    'slug' => \Str::slug('La Ceiba', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Concepción de Trujllo', 
		    'slug' => \Str::slug('La Concepción de Trujllo', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Mesa de Esnujaque', 
		    'slug' => \Str::slug('La Mesa de Esnujaque', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Puerta', 
		    'slug' => \Str::slug('La Puerta', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Quebrada', 
		    'slug' => \Str::slug('La Quebrada', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Mendoza Fría', 
		    'slug' => \Str::slug('Mendoza Fría', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Meseta de Chimpire', 
		    'slug' => \Str::slug('Meseta de Chimpire', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Monay', 
		    'slug' => \Str::slug('Monay', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Motatán', 
		    'slug' => \Str::slug('Motatán', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Pampán', 
		    'slug' => \Str::slug('Pampán', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Pampanito', 
		    'slug' => \Str::slug('Pampanito', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Sabana de Mendoza', 
		    'slug' => \Str::slug('Sabana de Mendoza', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Lázaro', 
		    'slug' => \Str::slug('San Lázaro', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Ana de Trujillo', 
		    'slug' => \Str::slug('Santa Ana de Trujillo', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tostós', 
		    'slug' => \Str::slug('Tostós', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Trujillo', 
		    'slug' => \Str::slug('Trujillo', '-'),
		    'state_id' => 21,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Valera', 
		    'slug' => \Str::slug('Valera', '-'),
		    'state_id' => 21,
		    'capital' => 0
		]);


		# Vargas
		City::create([
		    'city' => 'Carayaca', 
		    'slug' => \Str::slug('Carayaca', '-'),
		    'state_id' => 22,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Litoral', 
		    'slug' => \Str::slug('Litoral', '-'),
		    'state_id' => 22,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Guaira', 
		    'slug' => \Str::slug('La Guaira', '-'),
		    'state_id' => 22,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Catia La Mar', 
		    'slug' => \Str::slug('Catia La Mar', '-'),
		    'state_id' => 22,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Macuto', 
		    'slug' => \Str::slug('Macuto', '-'),
		    'state_id' => 22,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Naiguatá', 
		    'slug' => \Str::slug('Naiguatá', '-'),
		    'state_id' => 22,
		    'capital' => 0
		]);


		# Yaracuy
		City::create([
		    'city' => 'Aroa', 
		    'slug' => \Str::slug('Aroa', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Boraure', 
		    'slug' => \Str::slug('Boraure', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Campo Elías de Yaracuy', 
		    'slug' => \Str::slug('Campo Elías de Yaracuy', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chivacoa', 
		    'slug' => \Str::slug('Chivacoa', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cocorote', 
		    'slug' => \Str::slug('Cocorote', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Farriar', 
		    'slug' => \Str::slug('Farriar', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Guama', 
		    'slug' => \Str::slug('Guama', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Marín', 
		    'slug' => \Str::slug('Marín', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Nirgua', 
		    'slug' => \Str::slug('Nirgua', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Sabana de Parra', 
		    'slug' => \Str::slug('Sabana de Parra', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Salom', 
		    'slug' => \Str::slug('Salom', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Felipe', 
		    'slug' => \Str::slug('San Felipe', '-'),
		    'state_id' => 23,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'San Pablo de Yaracuy', 
		    'slug' => \Str::slug('San Pablo de Yaracuy', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Urachiche', 
		    'slug' => \Str::slug('Urachiche', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Yaritagua', 
		    'slug' => \Str::slug('Yaritagua', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Yumare', 
		    'slug' => \Str::slug('Yumare', '-'),
		    'state_id' => 23,
		    'capital' => 0
		]);


		# Zulia
		City::create([
		    'city' => 'Bachaquero', 
		    'slug' => \Str::slug('Bachaquero', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Bobures', 
		    'slug' => \Str::slug('Bobures', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Cabimas', 
		    'slug' => \Str::slug('Cabimas', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Campo Concepción', 
		    'slug' => \Str::slug('Campo Concepción', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Campo Mara', 
		    'slug' => \Str::slug('Campo Mara', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Campo Rojo', 
		    'slug' => \Str::slug('Campo Rojo', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Carrasquero', 
		    'slug' => \Str::slug('Carrasquero', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Casigua', 
		    'slug' => \Str::slug('Casigua', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Chiquinquirá', 
		    'slug' => \Str::slug('Chiquinquirá', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Ciudad Ojeda', 
		    'slug' => \Str::slug('Ciudad Ojeda', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Batey', 
		    'slug' => \Str::slug('El Batey', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Carmelo', 
		    'slug' => \Str::slug('El Carmelo', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Chivo', 
		    'slug' => \Str::slug('El Chivo', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Guayabo', 
		    'slug' => \Str::slug('El Guayabo', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Mene', 
		    'slug' => \Str::slug('El Mene', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'El Venado', 
		    'slug' => \Str::slug('El Venado', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Encontrados', 
		    'slug' => \Str::slug('Encontrados', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Gibraltar', 
		    'slug' => \Str::slug('Gibraltar', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Isla de Toas', 
		    'slug' => \Str::slug('Isla de Toas', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Concepción del Zulia', 
		    'slug' => \Str::slug('La Concepción del Zulia', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Paz', 
		    'slug' => \Str::slug('La Paz', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'La Sierrita', 
		    'slug' => \Str::slug('La Sierrita', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Lagunillas del Zulia', 
		    'slug' => \Str::slug('Lagunillas del Zulia', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Las Piedras de Perijá', 
		    'slug' => \Str::slug('Las Piedras de Perijá', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Los Cortijos', 
		    'slug' => \Str::slug('Los Cortijos', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Machiques', 
		    'slug' => \Str::slug('Machiques', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Maracaibo', 
		    'slug' => \Str::slug('Maracaibo', '-'),
		    'state_id' => 24,
		    'capital' => 1
		]);
		City::create([
		    'city' => 'Mene Grande', 
		    'slug' => \Str::slug('Mene Grande', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Palmarejo', 
		    'slug' => \Str::slug('Palmarejo', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Paraguaipoa', 
		    'slug' => \Str::slug('Paraguaipoa', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Potrerito', 
		    'slug' => \Str::slug('Potrerito', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Pueblo Nuevo del Zulia', 
		    'slug' => \Str::slug('Pueblo Nuevo del Zulia', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Puertos de Altagracia', 
		    'slug' => \Str::slug('Puertos de Altagracia', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Punta Gorda', 
		    'slug' => \Str::slug('Punta Gorda', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Sabaneta de Palma', 
		    'slug' => \Str::slug('Sabaneta de Palma', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Francisco', 
		    'slug' => \Str::slug('San Francisco', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San José de Perijá', 
		    'slug' => \Str::slug('San José de Perijá', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Rafael del Moján', 
		    'slug' => \Str::slug('San Rafael del Moján', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'San Timoteo', 
		    'slug' => \Str::slug('San Timoteo', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Bárbara Del Zulia', 
		    'slug' => \Str::slug('Santa Bárbara Del Zulia', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Cruz de Mara', 
		    'slug' => \Str::slug('Santa Cruz de Mara', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Cruz del Zulia', 
		    'slug' => \Str::slug('Santa Cruz del Zulia', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Santa Rita', 
		    'slug' => \Str::slug('Santa Rita', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Sinamaica', 
		    'slug' => \Str::slug('Sinamaica', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tamare', 
		    'slug' => \Str::slug('Tamare', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Tía Juana', 
		    'slug' => \Str::slug('Tía Juana', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Villa del Rosario', 
		    'slug' => \Str::slug('Villa del Rosario', '-'),
		    'state_id' => 24,
		    'capital' => 0
		]);

		# Dependencias Federales
		City::create([
		    'city' => 'Archipiélago Los Roques', 
		    'slug' => \Str::slug('Archipiélago Los Roques', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Archipiélago Los Monjes', 
		    'slug' => \Str::slug('Archipiélago Los Monjes', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Isla La Tortuga y Cayos adyacentes', 
		    'slug' => \Str::slug('Isla La Tortuga y Cayos adyacentes', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Isla La Sola', 
		    'slug' => \Str::slug('Isla La Sola', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Islas Los Testigos', 
		    'slug' => \Str::slug('Islas Los Testigos', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Islas Los Frailes', 
		    'slug' => \Str::slug('Islas Los Frailes', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Isla La Orchila', 
		    'slug' => \Str::slug('Isla La Orchila', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Archipiélago Las Aves', 
		    'slug' => \Str::slug('Archipiélago Las Aves', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Isla de Aves', 
		    'slug' => \Str::slug('Isla de Aves', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Isla La Blanquilla', 
		    'slug' => \Str::slug('Isla La Blanquilla', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Isla de Patos', 
		    'slug' => \Str::slug('Isla de Patos', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
		City::create([
		    'city' => 'Islas Los Hermanos', 
		    'slug' => \Str::slug('Islas Los Hermanos', '-'),
		    'state_id' => 25,
		    'capital' => 0
		]);
    }
}
