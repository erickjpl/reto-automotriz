<?php

use Illuminate\Database\Seeder;
use App\Models\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->delete();

        State::create([
			'state' => 'Amazonas',
			'slug' => \Str::slug('Amazonas', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Anzoátegui',
			'slug' => \Str::slug('Anzoátegui', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Apure',
			'slug' => \Str::slug('Apure', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Aragua',
			'slug' => \Str::slug('Aragua', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Barinas',
			'slug' => \Str::slug('Barinas', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Bolívar',
			'slug' => \Str::slug('Bolívar', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Carabobo',
			'slug' => \Str::slug('Carabobo', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Cojedes',
			'slug' => \Str::slug('Cojedes', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Delta Amacuro',
			'slug' => \Str::slug('Delta Amacuro', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Distrito Capital',
			'slug' => \Str::slug('Distrito Capital', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Falcón',
			'slug' => \Str::slug('Falcón', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Guárico',
			'slug' => \Str::slug('Guárico', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Lara',
			'slug' => \Str::slug('Lara', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Mérida',
			'slug' => \Str::slug('Mérida', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Miranda',
			'slug' => \Str::slug('Miranda', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Monagas',
			'slug' => \Str::slug('Monagas', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Nueva Esparta',
			'slug' => \Str::slug('Nueva Esparta', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Portuguesa',
			'slug' => \Str::slug('Portuguesa', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Sucre',
			'slug' => \Str::slug('Sucre', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Táchira',
			'slug' => \Str::slug('Táchira', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Trujillo',
			'slug' => \Str::slug('Trujillo', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Vargas',
			'slug' => \Str::slug('Vargas', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Yaracuy',
			'slug' => \Str::slug('Yaracuy', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Zulia',
			'slug' => \Str::slug('Zulia', '-'),
			'country_id' => 1
		]);
		State::create([
			'state' => 'Dependencias Federales',
			'slug' => \Str::slug('Dependencias Federales', '-'),
			'country_id' => 1
		]);
    }
}
