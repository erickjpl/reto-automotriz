<?php

use Illuminate\Database\Seeder;
use App\Models\Layout;

class LayoutsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('layouts')->delete();
       
        Layout::create([
            'layout' => 'uno',
            'slug'   => \Str::slug('uno', '-')
        ]);

        Layout::create([
            'layout' => 'dos',
            'slug'   => \Str::slug('dos', '-')
        ]);

        Layout::create([
            'layout' => 'tres',
            'slug'   => \Str::slug('tres', '-')
        ]);
    }
}
