<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Concessionaire;
use Faker\Generator as Faker;

$factory->define(Concessionaire::class, function (Faker $faker) {
    return [
		'name' 		=> $faker->company,
		'phone' 	=> '+58 ('.Arr::random([212, 412, 414, 416, 424, 426]).') '.rand(1000000, 9999999),
		'address' 	=> $faker->address,
		'zip_code' 	=> $faker->postcode,
		'city_id' 	=> App\Models\City::all()->random()->id,
        'layout_id' => App\Models\Layout::all()->random()->id
    ];
});
