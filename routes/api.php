<?php

use Illuminate\Http\Request;

Route::prefix('auth')->group(function () {
    Route::post('register', 'Api\AuthController@register');
    Route::post('login',    'Api\AuthController@login');
    Route::get('refresh',   'Api\AuthController@refresh');
    
    Route::middleware('auth:api')->group(function () {
        Route::get('user',      'Api\AuthController@user');
        Route::post('logout',   'Api\AuthController@logout');
    });
});

Route::middleware('auth:api')->group(function () {
    Route::get('employees', function() {
        return App\Models\Employee::all();
    });

    Route::get('employees/{id}', function($id) {
        return App\Models\Employee::find($id);
    });
});

Route::get('test', function() {

    return response()->json([
    	'data' => App\Models\User::with(['employee'])->get()
    ], 200);
});
