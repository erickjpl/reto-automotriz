<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use Tymon\JWTAuth\Exceptions\JWTException;

/**
 *
 * AuthController uso:
 *      autenticar al usuario
 *      cerrar session
 *      registrar un usuario con el rol cliente tipo visitante
 *      registrar usuarios empleados (ADMIN - GERENTE GENERAL - GERENTE ADMINITRATIVO)
 *      asignar permisos a los roles  (ADMIN - GERENTE GENERAL)
 *      crear, vizualizar, modificar, eliminar roles (ADMIN - GERENTE GENERAL)
 *      modificar estatus del empleado (GERENTE GENERAL - GERENTE ADMINITRATIVO)
 *      actualizar contraseña del usuario autenticado
 *      los usuarios pueden realizar CRUD de su perfil 
 */
class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if ($token = $this->guard()->attempt($credentials)) 
            return response()->json(['status' => 'success'], 200)->header('Authorization', $token);

        return response()->json(['error' => 'Credenciales invalidas.'], 401);
    }

    public function logout()
    {
        try {
            $this->guard()->logout();

            return response()->json([
                'status'    => 'success',
                'message'   => 'Cierre de sesión exitoso.'
            ], 200);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => 'error',
                'message' => 'Hubo un error al destruir la sesion.'
            ], 500);
        }
    }

    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            return response()->json(['status' => 'successs'], 200)->header('Authorization', $token);
        }

        return response()->json(['error' => 'refresh_token_error'], 401);
    }

    private function guard()
    {
        return Auth::guard();
    }

    public function user(Request $request)
    {
        return response()->json([
            'status'    => 'success',
            'data'      => User::find(Auth::user()->id)
        ]);
    }

    private function __user(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return $user;
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->__user($request);

        $user->assignRoles('cliente');

        return response()->json([
            'status'    => 'success',
            'message'   => 'Registro exitoso.',
            'data'      => $user
        ], 200);
    }

    public function employee(EmployeeRequest $request)
    {
        try {
            $user = $this->__user($request);

            $user->assignRoles($request->role);

            $employee = $user->employee->create($request);

            $employee->contracts->create($request);

            return response()->json([
                'status'    => 'success',
                'message'   => 'Registro exitoso.',
#                'data'      => $user->with('employee', 'contracts');
            ], 200);
        } catch(Exception $e) {
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
            ], 500); 
        }
    }

    public function example(Request $request)
    {
        try {
            $user = $this->__user($request);

            $user->assignRoles($request->role);

            return response()->json([
                'status'    => 'success',
                'message'   => 'Registro exitoso.',
                'data'      => $user
            ], 200);
        } catch(Exception $e) {
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
                'data'      => '$user'
            ], 500); 
        }
    }
}
