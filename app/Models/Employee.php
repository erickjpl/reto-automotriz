<?php

namespace App\Models;

use App\Models\User;
use App\Models\Contract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use softDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'dni', 'phone', 'birth', 'status'
    ];

    /**
	 * Get the user that owns the employee.
	 */
	public function user()
	{
	    return $this->belongsTo(User::class);
	}

    /**
     * Get the contracts for the employee.
     */
    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }
}
