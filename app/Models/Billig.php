<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Billig extends Model
{
    use softDeletes;

    public function vehicleDetails()
    {
        return $this->hasMany(VehicleDetail::class);
    }

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }
}
