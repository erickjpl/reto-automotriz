<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Concessionaire extends Model
{
    use softDeletes;

    public function emplayees()
    {
        return $this->hasMany(Emplayee::class);
    }
}
