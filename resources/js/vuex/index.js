import Vue  from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

/* MODULES */
import employee from '@/vuex/modules/EmployeeModule'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        employee
    },
    state: {
        layout: 'default',
        apiToken: localStorage.getItem( 'api_token' ),
        concessionaires: [
            { concessionaire: 'mi-auto',        layout: 'uno' },
            { concessionaire: 'el-automovil',   layout: 'uno' },
            { concessionaire: 'carrito-mio',    layout: 'dos' }
        ]
    },
    getters: {
        token:  state => { return { headers: { 'Authorization':  `Bearer ${state.apiToken}` }}},
        layout: state => state.layout,
        concessionaires: state => state.concessionaires
    },
    actions: {
        changeLayout({commit}, payload) {
            commit( 'layout', payload )
        }
    },
    mutations: {
        layout( state, payload ) {
            Vue.set( state, 'layout', payload )
        }
    },
    plugins: [createPersistedState()]
})
