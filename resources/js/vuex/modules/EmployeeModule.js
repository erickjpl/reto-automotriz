import { EmployeeRepository } from '@/repositories/EmployeeRepository'

export default {
    namespaced: true,
    state: {
        employees  : [],
        employee   : null,
        errors     : [],
    },
    actions: {
        async getAll({commit}) {
            commit( 'request' )
            let data = await EmployeeRepository.getAll( this.getters.token )
                .then( response => commit( 'employees', response.data ))
                .catch( error   => commit( 'error', error ))
        },
        async get({commit}, id) {
            let data = await EmployeeRepository.get( this.getters.token, id )
                .then( response => commit( 'employee', response.data ))
                .catch( error   => commit( 'error', error ))
        },
        async update({commit}, param) {
            commit( 'hotel_request' )
            let data = await EmployeeRepository.update( this.getters.token, param )
            .then(response => commit( 'hotel_success', response.data ))
            .catch(error => {
                commit( 'error_store', error.data.errors )
                return Promise.reject(error)
            })
        },
        async store({commit}, param) {
            commit( 'hotel_request' )
            let data = await EmployeeRepository.add( this.getters.token, param )
            .then(() => commit( 'hotel_success' ))
            .catch(error => {
                commit( 'error_store', error.data.errors )
                return Promise.reject(error)
            })
        },
        async destroy({commit}, param) {
            commit( 'hotel_request' )
            let data = await EmployeeRepository.remove( this.getters.token, param )
            .then(() => commit( 'hotel_success' ))
            .catch(error => {
                commit( 'hotel_error' )
                return Promise.reject(error)
            })
        }
    },
    getters: {
        employees: state => state.employees,
        employee : state => state.employee,
    },
    mutations: {
        employees( state, payload ) {
            Vue.set( state, 'employees', payload )
            //state.employees.push(payload)
        },
        employee( state, payload ) {
            Vue.set( state, 'employee', payload )
            //state.employees.push(payload)
        },
        error( state, payload ) {
            Vue.set( state, 'errors', payload )
        },
        request() {
            Vue.set( this.state, 'apiToken', localStorage.getItem( 'api_token' ) )
        },
        /*hotel_success( state, data ) {
            Vue.set(state, 'hotel', data)
            Vue.set(state, 'status', 'success')
        },
        hotel_error( state ) {
            Vue.set(state, 'status', 'error')
        },
        billing_plan_hotels( state, data ) {
            Vue.set(state, 'billingPlanHotels', data)
        },
        flag( state, data ) {
            Vue.set(state, 'flag', data)
        }*/
    }
}
