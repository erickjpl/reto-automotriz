require('./bootstrap');

import 'es6-promise/auto'
import axios from 'axios'
import Vue from 'vue'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import store from '@/vuex'

import vuetify from '@/plugins/vuetify'
import '@/plugins/validator'

import Index from '@/pages/Index'
import auth from '@/config/auth'
import router from '@/config/router'

window.Vue = Vue
Vue.router = router
Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(VueAxios, axios)
Vue.use(VueAuth, auth)
axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api/`

Vue.component('index', Index)

const app = new Vue({
	el: '#app',
	router,
    store,
	vuetify
});
