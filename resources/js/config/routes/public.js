import Home     from '@/pages/Home'
import About    from '@/pages/About'
import Login    from '@/pages/Login'
import Register from '@/pages/Register'
import Person   from '@/pages/Person'

export default [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/about',
        name: 'about',
        component: About,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/test',
        name: 'test',
        component: Person,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    }
]
