import LDHome  from '@/pages/layouts/dos/Home'
import LDIndex from '@/pages/layouts/dos/Index'

export default [
    {
        path: '/l2/concessionaire/:concessionaire',
        name: 'layout.dos.index',
        component: LDIndex,
        props: true,
        meta: {
            auth: undefined
        },
        redirect: { name: 'layout.dos.home' },
        children: [
            {
                path: 'home',
                name: 'layout.dos.home',
                components: {
                    ldos: LDHome
                },
                meta: {
                    auth: undefined
                }
            }
        ]
    }
]
