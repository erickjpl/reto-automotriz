import LUHome  from '@/pages/layouts/uno/Home'
import LUBlog  from '@/pages/layouts/uno/Blog'
import LUCart  from '@/pages/layouts/uno/Cart'
import LUIndex from '@/pages/layouts/uno/Index'

export default [
    {
        path: '/l1/concessionaire/:concessionaire',
        name: 'layout.uno.index',
        component: LUIndex,
        props: true,
        meta: {
            auth: undefined
        },
        redirect: { name: 'layout.uno.home' },
        children: [
            {
                path: 'home',
                name: 'layout.uno.home',
                components: {
                    luno: LUHome
                },
                meta: {
                    auth: undefined
                }
            },
            {
                path: 'blog',
                name: 'layout.uno.blog',
                components: {
                    luno: LUBlog
                },
                meta: {
                    auth: undefined
                }
            }, 
            {
                path: 'cart',
                name: 'layout.uno.cart',
                components: {
                    luno: LUCart
                },
                meta: {
                    auth: undefined
                }
            }
        ]
    }
]
