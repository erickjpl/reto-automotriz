import Index from '@/pages/dashboard/Index'
import Dashboard from '@/pages/user/Dashboard'
import AdminDashboard from '@/pages/dashboard/Dashboard'

export default [
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Index,
        //redirect: AdminDashboard,
        meta: {
            auth: {
                roles: ['admin', 'gerente', 'empleado'],
                redirect: {
                    name: 'login'
                },
                forbiddenRedirect: '/403'
            }
        },
        children: [
            {
                path: 'admin',
                name: 'dashboard.admin',
                components: {
                    dashboard: AdminDashboard
                },
                meta: {
                    auth: {
                        roles: 'admin',
                        redirect: {
                            name: 'login'
                        },
                        forbiddenRedirect: '/403'
                    }
                }
            }, 
            {
                path: 'user',
                name: 'dashboard.user',
                components: {
                    dashboard: Dashboard
                },
                meta: {
                    auth: {
                        roles: ['gerente', 'empleado'],
                        redirect: {
                            name: 'login'
                        },
                        forbiddenRedirect: '/403'
                    }
                }
            }
        ]
    }
]
