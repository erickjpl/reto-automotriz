import VueRouter from 'vue-router'

import PublicRoutes    from '@/config/routes/public'
import DashboardRoutes from '@/config/routes/dashboard'
import LayoutUno       from '@/config/routes/layouts/uno'
import LayoutUDos      from '@/config/routes/layouts/dos'

const routes = [
    ...LayoutUno,
    ...LayoutUDos,
    ...PublicRoutes,
    ...DashboardRoutes
]

const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
})

export default router
