import Service from '@/repositories/config/RepositoryFactory'

const API_URL = 'employees'

export const EmployeeRepository = new Service(API_URL);
