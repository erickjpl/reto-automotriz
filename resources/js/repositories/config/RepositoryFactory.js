import Repository from '@/repositories/config/Repository'

export default class RepositoryFactory {

    constructor(url) { this.url = `${process.env.MIX_APP_URL}/api/${url}` }

    getAll(token) {
        return Repository.get( this.url, token )
            .then(  (response)  => Promise.resolve(response) )
            .catch( (error)     => Promise.reject(error.response) )
    }

    get(token, id) {
        return Repository.get( `${this.url}/${id}`, token )
            .then(  (response)  => Promise.resolve(response) )
            .catch( (error)     => Promise.reject(error.response) )
    }

    add(token, data) {
        return Repository.post( this.url, data, token )
            .then(  (response)  => Promise.resolve(response) )
            .catch( (error)     => Promise.reject(error.response) )
    }

    update(token, data) {
        return Repository.put( `${this.url}/${data.id}`, data, token )
            .then(  (response)  => Promise.resolve(response) )
            .catch( (error)     => Promise.reject(error.response) )
    }

    remove(token, id) {
        return Repository.delete( `${this.url}/${id}`, token )
            .then(  (response)  => Promise.resolve(response) )
            .catch( (error)     => Promise.reject(error.response) )
    }
}
